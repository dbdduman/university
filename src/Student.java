import java.util.ArrayList;
import java.util.List;

public class Student {
    private String name;
    private String major;
    private int numCreditHours;
    private List<Course> courses;

    public Student(String name, String major, int numCreditHours) {
        this.name = name;
        this.major = major;
        this.numCreditHours = numCreditHours;
        this.courses = new ArrayList<Course>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public int getNumCreditHours() {
        return numCreditHours;
    }

    public void setNumCreditHours(int numCreditHours) {
        this.numCreditHours = numCreditHours;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void addCourse(Course course) {
        courses.add(course);
    }

    public void removeCourse(Course course) {
        courses.remove(course);
    }
}
