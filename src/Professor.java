import java.util.ArrayList;
import java.util.List;

public class Professor {
    private String name;
    private String department;
    private int yearsExperience;
    private List<Course> courses;

    public Professor(String name, String department, int yearsExperience) {
        this.name = name;
        this.department = department;
        this.yearsExperience = yearsExperience;
        this.courses = new ArrayList<Course>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department){
        this.department = department;
    }

}
