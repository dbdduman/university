import java.util.ArrayList;
import java.util.List;

public class Department {
    private String name;
    private int numStudents;
    private List<Professor> professors;
    private List<Course> courses;

    public Department(String name, int numStudents) {
        this.name = name;
        this.numStudents = numStudents;
        this.professors = new ArrayList<Professor>();
        this.courses = new ArrayList<Course>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumStudents() {
        return numStudents;
    }

    public void setNumStudents(int numStudents) {
        this.numStudents = numStudents;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public List<Professor> getProfessors() {
        return professors;
    }
    public void addCourses(Course course){
        this.courses.add(course);
    }
    public void removeCourses(Course course){
        this.courses.remove(course);
    }
    public void addProfessor(Professor professor){
        this.professors.add(professor);
    }
    public void removeProfessor(Professor professor){
        this.professors.remove(professor);
    }
}