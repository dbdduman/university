import java.util.ArrayList;
import java.util.List;

public class University {
    private String name;
    private String location;
    private int numStudents;
    private List<Department> departments;

    public University(String name, String location, int numStudents) {
        this.name = name;
        this.location = location;
        this.numStudents = numStudents;
        this.departments = new ArrayList<Department>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getNumStudents() {
        return numStudents;
    }

    public void setNumStudents(int numStudents) {
        this.numStudents = numStudents;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void addDepartment(Department department) {
        departments.add(department);
    }

    public void removeDepartment(Department department) {
        departments.remove(department);
    }
}