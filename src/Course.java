import java.util.ArrayList;
import java.util.List;

public class Course {
    private String name;
    private String courseNumber;
    private int numStudents;
    private List<Student> students;

    public Course(String name, String courseNumber, int numStudents) {
        this.name = name;
        this.courseNumber = courseNumber;
        this.numStudents = numStudents;
        this.students = new ArrayList<Student>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourseNumber() {
        return courseNumber;
    }

    public void setCourseNumber(String courseNumber) {
        this.courseNumber = courseNumber;
    }

    public int getNumStudents() {
        return numStudents;
    }

    public void setNumStudents(int numStudents) {
        this.numStudents = numStudents;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void addStudent(Student student) {
        students.add(student);
    }

    public void removeStudent(Student student) {
        students.remove(student);
    }
}
