# university


#
The University class would have a "has-a" relationship with the Department class, meaning that a university "has" one or more departments.
The Department class would also have a "has-a" relationship with the Course class, meaning that a department "has" one or more courses. 
Similarly, the Course class would have a "has-a" relationship with the Student class, meaning that a course "has" one or more students, 
and the Professor class would have a "has-a" relationship with the course class, meaning that a professor "has" one or more courses.
#
#
![img.png](img.png)
#